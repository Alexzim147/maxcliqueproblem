import numpy as np
import scipy.stats as sps
from collections import defaultdict
from itertools import combinations


class CliqueSolver:
    def __init__(self, graph=None, n=8, p=0.5):
        if graph is None:
            self.gen_graph(n, p)
        else:
            self.graph = graph
            self.n = graph.shape[0]

        self.methods = {'BK': self.bron_kerbosch,
                        'BKDO': self.bron_kerbosch_DO,
                        'stupid': self.stupid_solve,
                        'tomita': self.tomita,
                        'tomitaDO': self.tomita_DO}
        self.max_clique = set()
        self.calls_number = 0

    def gen_graph(self, n, p):
        ''' use to generate a random graph
        '''

        rv = sps.bernoulli(p)
        graph = np.zeros(shape=(n, n), dtype=int)
        for idx in range(n):
            graph[idx][:idx] = rv.rvs(size=(idx))
        self.graph = graph + graph.transpose()
        self.n = n

    def show_graph(self):
        ''' shows graph by printing its matrix
        '''

        print(self.graph + np.eye(self.n, dtype=int))

    def get_neighbours(self, vert):
        ''' returns neighbours of the vertex
        '''

        return set(*np.where(self.graph[vert, :] == 1))

    def is_clique(self, sub_graph):
        ''' returns True if subgraph is clique
        '''
        return np.sum(self.graph[np.meshgrid(tuple(sub_graph), 
                                              tuple(sub_graph))]) \
                                        == \
                    len(sub_graph) * (len(sub_graph) - 1)

    def is_maximal(self, sub_graph):
        ''' returns True if subgraph is maximal clique
        '''
        graph = (self.graph + np.eye(self.n, dtype=int))[tuple(sub_graph),]
        graph = graph[:, tuple(frozenset(np.arange(self.n)) - set(sub_graph)),]
        return not np.any(np.prod(graph, axis=0))

    def degeneracy_ordering(self, candidates):
        ''' returns vertices in degeneracy ordering
            (by the number of neighbours)
        '''
        
        self.neigbours_number = np.argsort(np.sum(self.graph[tuple(candidates),], axis=0))[::-1]
        return self.neigbours_number[tuple(candidates),]

    def get_max_clique(self, method='tomita'):
        ''' Returns a maximal clique in the graph.
            Default graph is a random graph with size = 8 and
            p = 0.5.
            You can choose the method to find a maximal clique
            from this list: 
        '''

        if not method in self.methods:
            raise 'Error in method name'

        self.calls_number = 0
        self.max_clique = set()

        if method == 'stupid':
            self.stupid_solve()
        else:
                # start with empty USED and COMBSUM in every algorithm
            self.methods[method](combsum=set(),
                                 candidates=set(np.arange(0, self.n)),
                                 used=set())
        return self.max_clique

    def stupid_solve(self):
        ''' finds maximal clique using brute force
            don't use it on large graphs
        '''

        for size in range(1, self.n + 1):
            for sub_graph in combinations(np.arange(self.n), size):
                if self.is_clique(sub_graph) and self.is_maximal(sub_graph):
                    self.max_clique = self.max_clique | {frozenset(sub_graph)}

    def bron_kerbosch(self, combsum, candidates, used):
        '''Bron-Kerbosch algorithm'''

        self.calls_number += 1
        if len(candidates) == 0 and len(used) == 0:
            self.max_clique = self.max_clique | {frozenset(combsum)}
            return

        for vert in candidates.copy():
            self.bron_kerbosch(combsum.copy() | {vert},
                               candidates.copy() & self.get_neighbours(vert),
                               used.copy() & self.get_neighbours(vert))
            candidates.remove(vert)
            used.add(vert)

    def bron_kerbosch_DO(self, combsum, candidates, used):
        '''Bron-Kerbosch algorithm using degeneracy ordering
        '''

        self.calls_number += 1
        if len(candidates) == 0 and len(used) == 0:
            self.max_clique = self.max_clique | {frozenset(combsum)}
            return

        for vert in self.degeneracy_ordering(candidates.copy()):
            self.bron_kerbosch(combsum.copy() | {vert},
                                  candidates.copy() & self.get_neighbours(vert),
                                  used.copy() & self.get_neighbours(vert))
            candidates.remove(vert)
            used.add(vert)

    def get_pivot(self, candidates, used):
        ''' Choose the pivot vertex from CANDIDATES ∪ USED
        '''

        neigbours = np.argsort(self.graph[tuple(candidates), ].sum(axis=0))[::-1]
        for idx in neigbours:
            if idx in used | candidates:
                return idx

    def tomita(self, combsum, candidates, used):
        '''Tomita algorithm
        '''

        self.calls_number += 1
        if len(candidates) == 0 and len(used) == 0:
            self.max_clique = self.max_clique | {frozenset(combsum)}
            return

        pivot = self.get_pivot(candidates,  used)

        for vert in candidates.copy() - self.get_neighbours(pivot):
            self.tomita(combsum.copy() | {vert},
                        candidates.copy() & self.get_neighbours(vert),
                        used.copy() & self.get_neighbours(vert))
            candidates.remove(vert)
            used.add(vert)

    def tomita_DO(self, combsum, candidates, used):
        '''Tomita algorithm with degeneracy ordering
        '''

        self.calls_number += 1
        if len(candidates) == 0 and len(used) == 0:
            self.max_clique = self.max_clique | {frozenset(combsum)}
            return

        pivot = self.get_pivot(candidates,  used)

        for vert in self.degeneracy_ordering(candidates.copy() - self.get_neighbours(pivot)):
            self.tomita(combsum.copy() | {vert},
                        candidates.copy() & self.get_neighbours(vert),
                        used.copy() & self.get_neighbours(vert))
            candidates.remove(vert)
            used.add(vert)
